/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluador;

import ufps.util.colecciones_seed.*;

/**
 *
 * @author estudiante
 */
public class Expresion {

    private ListaCD<String> expresiones = new ListaCD();

    public Expresion() {
    }

    public Expresion(String cadena) {

        String v[] = cadena.split(",");
        for (String dato : v) {
            this.expresiones.insertarAlFinal(dato);
        }
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
        String msg = "";
        for (String dato : this.expresiones) {
            msg += dato + "<->";
        }
        return msg;
    }
    
    // Método que retorna la expresión de forma infija
    public String getInfijo() throws Exception{
        if(!validarExpresion()) throw new Exception("La expresión es inválida");
        
        String result = "";
        for(String dato : this.expresiones){
            result += dato + " ";
        }
        return result;
    }

    // Método que retorna el valor de la expresión Prefija --> Notación polaca
    public String getPrefijo() throws Exception{
        if(!validarExpresion()) throw new Exception("La expresión es inválida");
        
        Pila<String> entrada = new Pila<>();
        Pila<String> operadores = new Pila<>();
        Pila<String> salida = new Pila<>();
        String result = "";
        
        // Agarrar la expresión al revés a través de una pila --> Facilidad para manejar Prefijo 
        for(String dato : this.expresiones){
            entrada.apilar(dato);
        }
        
        // recorrer la pila
        while(!entrada.esVacia()){
            String dato = entrada.desapilar();
            switch(dato){
                case "+":{
                    if(!operadores.esVacia() && (operadores.getTope().equals("-") || operadores.getTope().equals("*") || operadores.getTope().equals("/")))
                        salida.apilar(operadores.desapilar());
                    
                    operadores.apilar(dato);
                    break;
                }
                
                case "-":{
                    if(!operadores.esVacia() && (operadores.getTope().equals("*") || operadores.getTope().equals("/")))
                        salida.apilar(operadores.desapilar());
                    
                    operadores.apilar(dato);
                    break;
                }
                
                case "*":{
                    if(!operadores.esVacia() && operadores.getTope().equals("/"))
                        salida.apilar(operadores.desapilar());
                    
                    operadores.apilar(dato);
                    break;
                }
                
                case "/":{
                    operadores.apilar(dato);
                    break;
                }
                
                case "(":{
                    while(!operadores.esVacia()) salida.apilar(operadores.desapilar());
                    break;
                }
                
                case ")":{
                    operadores.apilar(dato);
                    break;
                }
                
                default:{
                    salida.apilar(dato);
                    break;
                }
            }
        }
        
        // Colocar los signos que deberían ir al comienzo del String a retornar
        while(!operadores.esVacia()) salida.apilar(operadores.desapilar());
        
        // Ajustar el String de retorno
        while(!salida.esVacia()){
            String tmp = salida.desapilar();
            if(!tmp.equals(")")) result += tmp + " ";
        }
        return result;
    }

    // Método que retorna la expresión Posfija o sufija --> Notación polaca inversa
    public String getPosfijo() throws Exception{
        if(!validarExpresion()) throw new Exception("La expresión es inválida");

        Pila<String> operadores = new Pila<>();
        String result = "";
        
        for(String dato : this.expresiones){
            switch(dato){
                case "(":{
                    operadores.apilar(dato);
                    break;
                }
                
                case ")":{
                    while(!operadores.getTope().equals("("))
                        result += operadores.desapilar() + " ";
                    
                    operadores.desapilar();
                    break;
                }
                
                case "+":{
                    while(!operadores.esVacia() && (operadores.getTope().equals("+") || operadores.getTope().equals("-") || operadores.getTope().equals("*") || operadores.getTope().equals("/")))
                        result += operadores.desapilar() + " ";
                    operadores.apilar(dato);
                    break;
                }
                
                case "-":{
                    while(!operadores.esVacia() && (operadores.getTope().equals("-") || operadores.getTope().equals("*") || operadores.getTope().equals("/")))
                        result += operadores.desapilar() + " ";
                    operadores.apilar(dato);
                    break;
                }
                
                case "*":{
                    while(!operadores.esVacia() && (operadores.getTope().equals("*") || operadores.getTope().equals("/")))
                        result += operadores.desapilar() + " ";
                    operadores.apilar(dato);
                    break;
                }
                
                case "/":{
                    while(!operadores.esVacia() && operadores.getTope().equals("/"))
                        result += operadores.desapilar() + " ";
                    operadores.apilar(dato);
                    break;
                }
                
                default:{
                    result += dato + " ";
                    break;
                }
            }
        }
        
        while(!operadores.esVacia())
            result += operadores.desapilar() + ' ';         
        return result;
    }

    // Método que retorna el valor de la expresión Posfija o sufija --> Notación polaca inversa
    public float getEvaluarPosfijo() {
        try {
            String [] v = getPosfijo().split(" ");
            Pila<String> pila = new Pila<>();
            for(String dato : v){
                switch(dato){
                    case "+":{
                        float op2 = Float.parseFloat(pila.desapilar());
                        float op1 = Float.parseFloat(pila.desapilar());
                        float tmp = op1 + op2;
                        pila.apilar(String.valueOf(tmp));
                        break;
                    }
                    
                    case "-":{
                        float op2 = Float.parseFloat(pila.desapilar());
                        float op1 = Float.parseFloat(pila.desapilar());
                        float tmp = op1 - op2;
                        pila.apilar(String.valueOf(tmp));
                        break;
                    }
                    
                    case "*":{
                        float op2 = Float.parseFloat(pila.desapilar());
                        float op1 = Float.parseFloat(pila.desapilar());
                        float tmp = op1 * op2;
                        pila.apilar(String.valueOf(tmp));
                        break;
                    }
                    
                    case "/":{
                        float op2 = Float.parseFloat(pila.desapilar());
                        float op1 = Float.parseFloat(pila.desapilar());
                        float tmp = op1 / op2;
                        pila.apilar(String.valueOf(tmp));
                        break;
                    }
                    
                    default:{
                        pila.apilar(dato);
                        break;
                    }
                }
            }
            return Float.parseFloat(pila.getTope());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return 0.0f;
        }
    }
    
    // Validar cantidad de paréntesis, signos, etc y sus posiciones respectivas
    private boolean validarExpresion(){
        Pila<String> stack = new Pila <> ();
        for(String dato : this.expresiones){
            switch(dato){
                case "+":{
                    if(stack.esVacia()) return false;
                    stack.desapilar();
                    break;
                }
                
                case "-":{
                    if(stack.esVacia()) return false;
                    stack.desapilar();
                    break;
                }
                
                case "*":{
                    if(stack.esVacia()) return false;
                    stack.desapilar();
                    break;
                }
                
                case "/":{
                    if(stack.esVacia()) return false;
                    stack.desapilar();
                    break;
                }
                
                case "(":{
                    if(!stack.esVacia() && !stack.getTope().equals("(")) return false;
                    stack.apilar(dato);
                    break;
                }
                
                case ")":{
                    if(stack.esVacia()) return false;
                    stack.desapilar();
                    if(stack.esVacia() || !stack.getTope().equals("(")) return false;
                    break;
                }
                
                default:{
                    try{
                        Float.parseFloat(dato);
                        stack.apilar(dato);
                    }catch(NumberFormatException ex){
                        System.out.println(ex.getMessage());
                    }
                    break;
                }
            }
        }
        return stack.getTamanio() == 1;
    }
}